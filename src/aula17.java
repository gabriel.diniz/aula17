import java.util.Scanner;

public class aula17 { // operadores lógicos - calcula idade

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);  // faz a leitura do que foi digitado

        final int iJ, iI; // constante idade jovem, idade idosa
        iJ = 17;
        iI = 60;

        //média mais de 17 e menos de 60;

        System.out.println("Digite a idade de uma pessoa: ");
        int idade = in.nextInt();

        if (idade <= iJ) { //  if se idade for menor ou igual
            System.out.println("A idade da pessoa informada é de um jovem."); // imprime a mensagem
        } else { // senão
            if (idade >= iI) { // if se idade for maior ou igual
                System.out.println("A idade da pessoa informada é de uma idosa"); // imprime a mensagem
            } else { //se não
                //se a idade > maior do que 17 EE < menor do que 60
                //então a condição é verdadeira!
                if( (idade > iJ)&&(idade < iI) ){
                        //if (idade >iJ) { // se idade maior que a idade de um jovem
                        // if (idade < iI) { // se idade menor que idade de um idoso

                        System.out.println("é uma pessoa de meia idade!"); // imprime a mensagem
                    }
            }
        }
    }
}
